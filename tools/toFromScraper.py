#!/usr/bin/env python -o

import sys
import re

f = open(sys.argv[1])
adr = re.compile('[a-z0-9._-]+@[a-z.-_]+\.[a-z]{2,}')
print sys.argv[1]

tos = set()
#grab the writer first, no need to unwrap
startTo = False
startCC = False
startFrom = False

for l in f:
    line = l.strip().lower()
    if re.match('^From:',l):
        a = re.search(adr,line)
        fr = line
        startFrom = True
    elif startFrom and l[0] == " ":
        fr += line
    elif startFrom:
        f.close()
        break

f = open(sys.argv[1])
for l in f:
    line = l.strip().lower()
    if re.match('^To: ',l):
#        print "To: found"
        t = line.strip()
        startTo = True
    elif startTo and l[0] == " ":
        t += line.strip()
    elif startTo:
        f.close()
        break

f = open(sys.argv[1])
for l in f:
    line = l.strip().lower()
    if re.match('^CC: ',l):
#        print "Cc found"
        c = line.strip()
        startCC = True
    elif startCC and l[0] == ' ':
        c += line.strip()
    elif startCC:
        break

try:
    x = re.search(adr,fr)
    sender = fr[x.start():x.end()].lower()
    print "<fr>",sender
    if "utf-8" in sender:
        raise Error
except:
#    print "no Sender found. probably a listserve."
    exit()

try:
    print "<to>", t
    for x in re.finditer(adr,t):
        if 'utf-8' not in t[x.start():x.end()]:
            tos.add(t[x.start():x.end()].lower())
    fOut = open(sys.argv[2],'w')
    for x in tos:
        fOut.write('{},{}\n'.format(sender,x))
#        print 'To: {},{}\n'.format(sender,x)
except:
    print "No recipient found."

try:
    print "<c>",c
    ccs = set()
    for x in re.finditer(adr,c):
        if "utf-8" not in c[x.start():x.end()]:
            ccs.add(c[x.start():x.end()].lower())
    for x in ccs:
        fOut.write('{},{}\n'.format(sender,x))
#        print 'Cc: {},{}\n'.format(sender,x)
except:
    print "no Cc recipients."

