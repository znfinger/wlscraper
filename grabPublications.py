 #!/usr/bin/env python

# 2016-10-17
# A tool designed to pull down the entire podesta email corpus from wikileaks in its current state
# "veritatis in virtute"
#
# /u/znfinger
#
import sys
import socket
import os
import urllib2
import re
import hashlib
import time
import email

socket.setdefaulttimeout(2)

pubDate = re.compile("emailid\/(?P<id>[0-9]+)\">(?P<date>[0-9]{4}\-[0-9]{2}\-[0-9]{2})<")
subject = re.compile("Subject.+emailid\/(?P<id>[0-9]+)\">(?P<subject>[^\<]+)<")
sendDate = re.compile("emailid\/(?P<id>[0-9]+)\">(?P<date>[0-9]{4}\-[0-9]{2}\-[0-9]{2} [012][0-9]\:[0-5][0-9])")

filedata = re.compile("fileid\/(?P<id>[0-9]+)\/(?P<filenum>[0-9]+)\">(?P<filename>[^<]+)")
sums = re.compile("(?P<md5>[0-9,A-F,a-f]{32})")
flagwords = open('/home/leucinedreams/git_repos/WLScraper/flags').read().strip().split('\n')
print "Looking for these words in emails:", '\t'.join(flagwords)
fout = open('masterTable_{}.tsv'.format(time.asctime().replace(' ','_').replace(':','')),'w')
UserAgent = None

class email:
        def __init__(self, emailid):
                self.emailID = emailid
                self.url = 'https://wikileaks.org/podesta-emails//get/{}'.format(emailid)
                self.subject = None
                self.sendDate = None
                self.pubDate = None
                self.podesta = None # leak number
                #the following will be subsumed into default email class
                self.att_id = None # attachment ID
                self.att_name = None
                self.att_data = None #attachment as binary
                self.att_md5 = None
                self.text = None
                self.att_flag = False
        def getAttachmentData(self):
                if self.att_flag == True:
                        success = False
                        self.att_url = 'https://www.wikileaks.org/podesta-emails//fileid/{}/{}'.format(self.emailID,self.att_id)
                        try:
                                self.att_data = urllib2.urlopen(self.att_url).read()
                                if self.att_data is not None:
                                        success = True
                        except:
                                print  "Attachment {} interrupted. Retrying.".format(self.att_name)
                        digest =  hashlib.md5()
                        digest.update(self.att_data)
                        self.att_md5 = digest.hexdigest()
                else:
                        return None
                                
        def __str__(self):
                #print  self.podesta , self.emailID , self.sendDate , self.pubDate , self.subject
                return '\t'.join(map(str,[self.podesta , self.emailID , self.sendDate , self.pubDate , self.subject]))

emails = {}
page = 1
#dates = set()
while True:
        print "grabbing page {}.".format(page)
        #opens the source page for email listings and downloads to grab the email#, publication date, send date, subject
        url = "https://wikileaks.org/podesta-emails/?q=&mfrom=&mto=&title=&notitle=&date_from=&date_to=&nofrom=&noto=&count=200&sort=6&page={}&#searchresult".format(page)
        #        print url
        success = False
        while success == False:
                try:
#                        print url
                        a = urllib2.urlopen(url,UserAgent).read()
                        success = True
                except:
                        print "trying to grab directory {} again.".format(page)

        if "Query failed. Please check your syntax." in a or page > 50:
                break
        a = a.split('\n')
        for line in a:
                pub = re.search(pubDate,line)
                sub = re.search(subject,line)
                sent = re.search(sendDate,line)
                if pub:
                        #publication date
                        if pub.group('id') not in emails:
                                emails[pub.group('id')] = email(pub.group('id'))
                        emails[pub.group('id')].pubDate = pub.group('date')
                        #dates.add(pub.group('date'))
                        #print line
                        continue
                if sub:
                        #subject
                        if sub.group('id') not in emails:
                                emails[sub.group('id')] = email(sub.group('id'))
                        emails[sub.group('id')].subject = sub.group('subject')
                        if any([re.search(" {} ".format(x.lower()),sub.group('subject').lower()) for x in flagwords]):
                                print "Keyword hit :", sub.group('subject')
                        #  print line
                        continue
                if sent:
                        #filter for any features here by adding additional components
                        #send date
                        if sent.group('id') not in emails:
                                emails[sent.group('id')] = email(sent.group('id'))
                        emails[sent.group('id')].sendDate = sent.group('date')
                        if re.search('2016\-0[4-9]', sent.group('date')):
                                print "++++++++++++++++++++++++++NEW 2016 EMAILS! ALERT!:", emails[sent.group('id')].url
			if re.search('2009\-0[2345]',sent.group('date')):
				print "The gap ({}):".format(sent.group('date')), emails[sent.group('id')].url
			if re.search('2011\-1[12]',sent.group('date')):
                                print "CVC investigation:", emails[sent.group('id')].url
                        #print line
                        #                                print sent.group('id'), sent.group('date'),emails[sent.group('id')].sendDate
                        continue
        page += 1

#rev_dates = {'podesta8': '2016-10-16', 'podesta9': '2016-10-17', 'podesta4': '2016-10-12', 'podesta5': '2016-10-13', 'podesta6': '2016-10-14', 'podesta7': '2016-10-15', 'podesta1': '2016-10-07', 'podesta2': '2016-10-10', 'podesta3': '2016-10-11', 'podesta10': '2016-10-18', 'podesta11': '2016-10-19'}

podesta = {'2016-10-31':'podesta23','2016-10-30':'podesta22','2016-10-29':'podesta21','2016-10-28':'podesta20','2016-10-27':'podesta19','2016-10-26':'podesta18','2016-10-25':'podesta17','2016-10-24':"podesta16",'2016-10-23':"podesta15",'2016-10-22':'podesta14','2016-10-21':'podesta13','2016-10-20': 'podesta12','2016-10-19': 'podesta11', '2016-10-18': 'podesta10', '2016-10-14': 'podesta6', '2016-10-15': 'podesta7', '2016-10-07': 'podesta1', '2016-10-17': 'podesta9', '2016-10-16': 'podesta8', '2016-10-11': 'podesta3', '2016-10-10': 'podesta2', '2016-10-13': 'podesta5', '2016-10-12': 'podesta4'}

#pull only from these dates
grabdate=set(["2016-10-31","2016-10-30","2016-10-29","2016-10-28"]) 

#uncomment next line to do a FULL pulldown of all material
#grabdate = set(podesta.keys())

print >> sys.stderr, "ordinalizing..."

print >> sys.stderr, "writing directory to file."
for emailID in emails.keys():
        emails[emailID].podesta = podesta[emails[emailID].pubDate]
        fout.write(str(emails[emailID])+"\n")

print "making folders for individual leaks."
#create folders for each leak
for x in podesta.values():
        if not os.path.exists(x):
            os.mkdir(x)

#grab text for all emails
print "grabbing email bodies and writing to .EML files"
for emailid in emails.keys():
        url = 'https://wikileaks.org/podesta-emails//get/{}'.format(emailid)
        if os.path.exists('{}/{}.EML'.format(emails[emailid].podesta,emails[emailid].emailID)):
#                print  >> sys.stdout,"skipping {}. Already exists.".format(emails[emailid].emailID)
                continue
        if emails[emailid].pubDate not in grabdate:
#                print >> sys.stdout, "skipping {}. Not in specified data range".format(emails[emailid].att_name)
                continue
        success = False
        while success == False:
                try:
                        url = emails[emailid].url
                        emails[emailid].text = urllib2.urlopen(url,UserAgent).read()
                        print "pulled down email, checking for kwords."
                        print "Email {} pulled down successfully.".format(emailid)
                        success = True
                except:
                        print "trying again to pull down email {}.".format(emailid)
        for wd in flagwords:
                if re.search(' {} '.format(wd.lower()), emails[emailid].text.lower()):
                        #changed to direct link of rendered email rather than source
                        print "keyword: {0} https://www.wikileaks.org/podesta-emails/emailid/{1}".format(wd,emailid)
        fOut = open('{}/{}.EML'.format(emails[emailid].podesta,emails[emailid].emailID),'w')
        fOut.write(emails[emailid].text)
        fOut.close()

#scrape for names of attachments
page = 1
md5s = set()
print "grabbing attachment directory."
data = ''
att_fout = open('masterTable_attachments_{}.tsv'.format(time.asctime().replace(' ','_').replace(':',"")),'w')
#pull all attachment name/url info from the WL directory
while True:
        success = False
        while success == False:
                try:
                        url = 'https://wikileaks.org/podesta-emails/?file=&count=200&page={}&#searchresult'.format(page)
                        print url
                        a = urllib2.urlopen(url,UserAgent).read()
                        success = True
                except:
                        print "trying to pull down attachments directory again."

        if "Query failed. Please check your syntax." in a:
                break
        else:
                data += a
                page += 1
                
data = filter(lambda x: re.search(filedata,x), data.split('\n'))

print "Creating attachments urls."
for line in data:
        data = re.search(filedata,line)
        if not data or data.group('id') not in emails or emails[data.group('id')].pubDate not in grabdate:
                print line
                continue
        emails[data.group('id')].att_id = data.group('filenum')
        emails[data.group('id')].att_name = data.group('filename')
        emails[data.group('id')].att_flag = True
        print emails[data.group('id')]
        
print "Grabbing attachments."

for x in emails.keys():
        if emails[x].att_flag == False:
                continue
        if len('{}/{}'.format(emails[x].podesta,emails[x].att_name)) > 150:
                name = '{}/{}_{}'.format(emails[x].podesta,emails[x].emailID,emails[x].att_name)[:150]
        else:
                name = '{}/{}_{}'.format(emails[x].podesta,emails[x].emailID,emails[x].att_name)
        if os.path.exists(name):
                continue #file already retrieved
        success = False
        failCount = 0
        while success == False:
                try:
                        print emails[x]
                        emails[x].getAttachmentData()
                        print "{}: Download was successful.". format(emails[x].att_name)
                        success = True
                except:
                        failCount += 1
                        if failCount > 20:
                                break
                        print "{}: Download failed. Trying again.". format(emails[x].att_name)
                        print emails[x].att_url



#        print "writing {}".format('\t'.join([emails[x].emailID , emails[x].att_id , emails[x].att_name]))
#        att_fout.write('\t'.join([emails[x].emailID , emails[x].att_id , email[x].att_name]) + '\n')

        print name
        try:
                fOut = open(name,'wb')
                fOut.write(emails[x].att_data)
        except:
                print emails[x].att_url
                print emails[x]
        
