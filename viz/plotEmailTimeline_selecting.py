#!/usr/bin/env python

import matplotlib as mpl
import matplotlib.pyplot as plt
import glob
import datetime
import re
import numpy as np

#input format: "Wed, 9 Dec 2015 08:10:20 -0500 (EST)"
('Wed', '9', 'Dec', '2015', '20', '08', '10', '20', '-0500', 'EST')
#destination format: "%a %b %d %H:%M:%S %Y"

#useful time things
#mo = dict(zip(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],range(12)))
#day  = dict(zip(['Mon','Tue','Wed','Thu','Fri','Sat','Sun'],range(7)))

timestamp = re.compile('^Date: (?P<dow>[MWTRFS][a-z]+), (?P<day>[0-9]+) (?P<mo>[A-Z][a-z][a-z]) (?P<y>(19|20)[0-9][0-9]) (?P<h>[0-9]{2,2}):(?P<m>[0-9]{2,2}):(?P<s>[0-9]{2,2}) (?P<tzd>[\+\-][0-9]{4,4})')

#takes .groups() from above, converts to epoch time
def timeConv(t):
    z = "{} {} {} {}:{}:{} {}".format(t[0],t[2],t[1],t[5],t[6],t[7],t[3])
#    print time.strptime(z)
#    print time.mktime(time.strptime(z))
    return datetime.datetime.strptime(z, "%a %b %d %H:%M:%S %Y")

kwords = ["russia"]
p = {}
excluded = {}
for fn in glob.iglob('podesta1/*EML'):
    print fn
    pod,email = fn.replace('.EML','').split('/')
#    include = True # uncomment this line to include all emails
    include = False
    for line in open(fn):
        z = re.search(timestamp,line.strip())
        if z:
            t = timeConv(z.groups())
        if any([x in line for x in kwords]):
            include = True
    if include == True:        
        print 'in'
        p.setdefault(pod,set()).add(t)
    else:
        print 'out'
        excluded.setdefault(pod,set()).add(t)

#print p

for release, entry in enumerate(p.values()):
    dates = mpl.dates.date2num(list(entry))
    plt.plot_date(dates, np.ones(len(entry)))
#    plt.plot_date(dates, 1 + release*np.ones(len(entry)))
#    x = release*np.ones(len(entry))
#    y = np.array(list(entry))
#    print len(x), len(y)
#    plt.plot(y,x,'ro')

plt.show()
